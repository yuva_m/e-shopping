package com.squareshift.shopping.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AddCartItemRequest {
    @JsonProperty(value = "product_id")
    Integer productId;

    Integer quantity;
}
