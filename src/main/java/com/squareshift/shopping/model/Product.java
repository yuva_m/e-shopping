package com.squareshift.shopping.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter @Setter @Builder
public class Product {
    private int id;

    private String name;

    private BigDecimal price;

    private String description;

    private String category;

    private String image;

    @JsonProperty(value = "discount_percentage")
    private BigDecimal discountPercentage;

    @JsonProperty(value = "weight_in_grams")
    private Long weightInGrams;
}
