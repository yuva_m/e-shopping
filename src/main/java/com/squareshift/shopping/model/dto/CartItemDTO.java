package com.squareshift.shopping.model.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @Builder
public class CartItemDTO {
    Integer productId;

    String description;

    Integer quantity;
}
