package com.squareshift.shopping.model.dto;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class CartItemsResponseDTO {
    private final List<CartItemDTO> cartItems = new ArrayList<>();
}
