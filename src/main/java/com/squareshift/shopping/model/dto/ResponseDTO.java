package com.squareshift.shopping.model.dto;

import lombok.Getter;

import java.util.Collections;
import java.util.List;

@Getter
public class ResponseDTO<T> {
    private final String status;

    private final String message;

    private final T items;

    public ResponseDTO(String status, String message, T items) {
        this.status = status;
        this.message = message;
        this.items = items;
    }

    public ResponseDTO(String message, T items) {
        this("success", message, items);
    }

    public List<ResponseDTO<T>> asList() {
        return Collections.singletonList(this);
    }
}
