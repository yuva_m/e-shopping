package com.squareshift.shopping.model;

import lombok.Builder;
import lombok.Getter;

@Getter @Builder
public class CartItem {
    Integer quantity;

    Product product;
}
