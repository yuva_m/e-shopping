package com.squareshift.shopping.service;

import com.squareshift.shopping.model.CartItem;
import com.squareshift.shopping.model.Product;
import com.squareshift.shopping.model.request.AddCartItemRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CartService {

    private final List<CartItem> cartItems = new ArrayList<>();

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public void addCartItem(AddCartItemRequest addCartItemRequest) {
        getCartItems().add(getCartItem(addCartItemRequest));
    }

    private CartItem getCartItem(AddCartItemRequest addCartItemRequest) {
        return CartItem.builder()
                .product(getProduct(addCartItemRequest))
                .quantity(addCartItemRequest.getQuantity())
                .build();
    }

    private Product getProduct(AddCartItemRequest addCartItemRequest) {
        return Product.builder().id(addCartItemRequest.getProductId()).build();
    }

    public void clearCart() {
        getCartItems().clear();
    }
}
