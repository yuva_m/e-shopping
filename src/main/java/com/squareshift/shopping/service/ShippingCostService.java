package com.squareshift.shopping.service;

import com.squareshift.shopping.entity.ShippingCost;
import com.squareshift.shopping.repository.ShippingCostRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class ShippingCostService {
    private final ShippingCostRepository shippingCostRepository;

    public ShippingCostService(ShippingCostRepository shippingCostRepository) {
        this.shippingCostRepository = shippingCostRepository;
    }

    public Optional<BigDecimal> getShippingCost(Long weight, Long distance) {
        List<ShippingCost> filteredShippingCosts = findShippingCost(weight, distance);

        if (filteredShippingCosts.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(new BigDecimal(filteredShippingCosts.get(0).getCost()));
    }

    private List<ShippingCost> findShippingCost(Long weight, Long distance) {
        List<ShippingCost> shippingCosts = shippingCostRepository.findAll();

        return shippingCosts.stream()
                .filter(getShippingCostPredicate(weight, distance)).collect(Collectors.toList());
    }

    private Predicate<ShippingCost> getShippingCostPredicate(Long weight, Long distance) {
        return shippingCost -> isWeightInRange(weight, shippingCost)
                                && isDistanceInRange(distance, shippingCost);
    }

    private boolean isDistanceInRange(Long distance, ShippingCost shippingCost) {
        return distance >= shippingCost.getMinimumDistance()
                && distance < shippingCost.getMaximumDistance();
    }

    private boolean isWeightInRange(Long weight, ShippingCost shippingCost) {
        return weight >= shippingCost.getMinimumWeight()
                && weight <= shippingCost.getMaximumWeight();
    }
}
