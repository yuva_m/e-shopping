package com.squareshift.shopping.controller;

import com.squareshift.shopping.model.CartItem;
import com.squareshift.shopping.model.dto.CartItemsResponseDTO;
import com.squareshift.shopping.model.dto.ResponseDTO;
import com.squareshift.shopping.model.request.AddCartItemRequest;
import com.squareshift.shopping.service.CartService;
import com.squareshift.shopping.util.mapper.ResponseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CartController {

    private final CartService cartService;

    private final ResponseMapper responseMapper;

    @Autowired
    public CartController(CartService cartService, ResponseMapper responseMapper) {
        this.cartService = cartService;
        this.responseMapper = responseMapper;
    }

    @GetMapping(path = "/cart/items", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ResponseDTO<CartItemsResponseDTO>>> fetchAllCartItems() {
        List<CartItem> cartItems = cartService.getCartItems();

        CartItemsResponseDTO cartItemsResponseDTO = responseMapper.map(cartItems);

        ResponseDTO<CartItemsResponseDTO> responseDTO
                = new ResponseDTO<>("Item available in the cart", cartItemsResponseDTO);
        return ResponseEntity.ok(responseDTO.asList());
    }

    @PostMapping(path = "/cart/item", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ResponseDTO<Object>>> addCartItem(@RequestBody AddCartItemRequest addCartItemRequest) {
        cartService.addCartItem(addCartItemRequest);

        ResponseDTO<Object> responseDTO = new ResponseDTO<>("Item has been added to cart", null);

        return ResponseEntity.ok(responseDTO.asList());
    }

    @PostMapping(path = "/cart/items", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ResponseDTO<Object>>> clearCart(@RequestParam String action) {
        ResponseDTO<Object> responseDTO;

        if (isInvalidAction(action)) {
            responseDTO = new ResponseDTO<>("Error", "Error state. Bad request.", null);
            return ResponseEntity.badRequest().body(responseDTO.asList());
        }

        cartService.clearCart();
        responseDTO = new ResponseDTO<>("All items have been removed from the cart !", null);
        return ResponseEntity.ok(responseDTO.asList());
    }

    private boolean isInvalidAction(String action) {
        return !"empty_cart".equals(action);
    }
}
