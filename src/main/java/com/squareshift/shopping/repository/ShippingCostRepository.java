package com.squareshift.shopping.repository;

import com.squareshift.shopping.entity.ShippingCost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShippingCostRepository extends JpaRepository<ShippingCost, Long> {
}
