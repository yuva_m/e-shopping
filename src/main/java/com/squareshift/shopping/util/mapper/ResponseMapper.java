package com.squareshift.shopping.util.mapper;

import com.squareshift.shopping.model.CartItem;
import com.squareshift.shopping.model.dto.CartItemDTO;
import com.squareshift.shopping.model.dto.CartItemsResponseDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ResponseMapper {
    public CartItemsResponseDTO map(List<CartItem> cartItems) {
        CartItemsResponseDTO cartItemsResponseDTO = new CartItemsResponseDTO();

        cartItems.forEach(cartItem -> cartItemsResponseDTO.getCartItems().add(map(cartItem)));

        return cartItemsResponseDTO;
    }

    private CartItemDTO map(CartItem cartItem) {
        return CartItemDTO.builder()
                .quantity(cartItem.getQuantity())
                .description(cartItem.getProduct().getDescription())
                .productId(cartItem.getProduct().getId())
                .build();
    }
}
