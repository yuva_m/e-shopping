package com.squareshift.shopping.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "shipping_cost", catalog = "ecom")
@Getter @Setter
@NoArgsConstructor
public class ShippingCost {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "weight_min")
    private int minimumWeight;

    @Column(name = "weight_max")
    private int maximumWeight;

    @Column(name = "distance_min")
    private int minimumDistance;

    @Column(name = "distance_max")
    private int maximumDistance;

    @Column(name = "cost")
    private int cost;
}
