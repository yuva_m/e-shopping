package com.squareshift.shopping.model.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ResponseDTOTest {
    @Test
    @DisplayName("Should return a singleton list")
    void asList_should_return_a_singleton_list() {
        List<ResponseDTO<String>> responseDTOList
                = new ResponseDTO<>("Test message", "test value").asList();

        assertThat(responseDTOList).isNotNull().hasSize(1);
    }

    @Test
    @DisplayName("Default response status is success")
    void default_response_status_value_is_success() {
        List<ResponseDTO<String>> responseDTOList
                = new ResponseDTO<>("Test message", "test value").asList();

        assertThat(responseDTOList).isNotNull();
        assertThat(responseDTOList.get(0).getStatus()).isEqualTo("success");
    }

    @Test
    @DisplayName("Message is mapped")
    void given_message_is_mapped() {
        String message = "Test message";
        List<ResponseDTO<String>> responseDTOList
                = new ResponseDTO<>(message, "test value").asList();

        assertThat(responseDTOList).isNotNull();
        assertThat(responseDTOList.get(0).getMessage()).isEqualTo(message);
    }

    @Test
    @DisplayName("Message is mapped")
    void given_response_is_mapped() {
        String message = "Test message";
        String response = "test response";
        List<ResponseDTO<String>> responseDTOList
                = new ResponseDTO<>(message, response).asList();

        assertThat(responseDTOList).isNotNull();
        assertThat(responseDTOList.get(0).getItems()).isEqualTo(response);
    }
}