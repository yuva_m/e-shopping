package com.squareshift.shopping.util.mapper;

import com.squareshift.shopping.model.CartItem;
import com.squareshift.shopping.model.Product;
import com.squareshift.shopping.model.dto.CartItemDTO;
import com.squareshift.shopping.model.dto.CartItemsResponseDTO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class ResponseMapperTest {

    @InjectMocks
    private ResponseMapper responseMapper;

    @Test
    @DisplayName("All cart items should be mapped to response")
    void cart_items_should_be_mapped_to_response_dto() {
        List<CartItem> cartItems = new ArrayList<>();
        cartItems.add(getNewCartItem(100, "T-Shirt", 5));
        cartItems.add(getNewCartItem(200, "Camera", 1));
        CartItemsResponseDTO responseDTO = responseMapper.map(cartItems);

        assertThat(responseDTO).isNotNull();
        List<CartItemDTO> cartItemDTOS = responseDTO.getCartItems();
        assertThat(cartItemDTOS).isNotNull().hasSize(cartItems.size());
        assertCartItemsEquals(cartItems, cartItemDTOS);
    }

    private CartItem getNewCartItem(int id, String description, int quantity) {
        Product product = getNewProduct(id, description);

        return CartItem.builder()
                .product(product)
                .quantity(quantity)
                .build();
    }

    private Product getNewProduct(int id, String description) {
        return Product.builder().id(id).description(description).build();
    }

    private void assertCartItemsEquals(List<CartItem> cartItems, List<CartItemDTO> cartItemDTOS) {
        for (CartItem cartItem : cartItems) {
            boolean matchFound = cartItemDTOS.stream().anyMatch(cartItemEqualsCartItemDTO(cartItem));
            assertTrue(matchFound);
        }
    }

    private Predicate<CartItemDTO> cartItemEqualsCartItemDTO(CartItem cartItem) {
        return cartItemDTO -> cartItemDTO.getProductId().equals(cartItem.getProduct().getId())
                && cartItemDTO.getQuantity().equals(cartItem.getQuantity())
                && cartItemDTO.getDescription().equals(cartItem.getProduct().getDescription());
    }
}