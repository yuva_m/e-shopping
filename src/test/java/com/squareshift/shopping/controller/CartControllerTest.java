package com.squareshift.shopping.controller;

import com.squareshift.shopping.model.CartItem;
import com.squareshift.shopping.model.dto.CartItemsResponseDTO;
import com.squareshift.shopping.model.dto.ResponseDTO;
import com.squareshift.shopping.model.request.AddCartItemRequest;
import com.squareshift.shopping.service.CartService;
import com.squareshift.shopping.util.mapper.ResponseMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CartControllerTest {
    @Mock
    private CartService cartService;

    @Captor
    private ArgumentCaptor<List<CartItem>> cartItemsCaptor;

    @Mock
    private ResponseMapper responseMapper;

    @InjectMocks
    private CartController cartController;

    @Test
    @DisplayName("Cart service is used to fetch cart items")
    void should_return_product_list_using_cart_service() {
        cartController.fetchAllCartItems();

        verify(cartService).getCartItems();
    }

    @Test
    @DisplayName("Response should be constructed using cart items")
    void response_should_be_constructed_from_cart_items() {
        List<CartItem> expectedCartItems = new ArrayList<>();
        when(cartService.getCartItems()).thenReturn(expectedCartItems);

        cartController.fetchAllCartItems();

        verify(responseMapper).map(cartItemsCaptor.capture());
    }

    @Test
    @DisplayName("Should respond with 200 ok after successfully fetching cart items")
    void should_respond_with_200_ok() {
        List<CartItem> expectedCartItems = new ArrayList<>();
        when(cartService.getCartItems()).thenReturn(expectedCartItems);

        ResponseEntity<List<ResponseDTO<CartItemsResponseDTO>>> responseEntity = cartController.fetchAllCartItems();

        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @DisplayName("Should respond with mapped values")
    void should_respond_with_mapped_values() {
        List<CartItem> cartItems = new ArrayList<>();
        CartItemsResponseDTO cartItemDTOS = new CartItemsResponseDTO();

        when(cartService.getCartItems()).thenReturn(cartItems);
        when(responseMapper.map(cartItems)).thenReturn(cartItemDTOS);

        ResponseEntity<List<ResponseDTO<CartItemsResponseDTO>>> responseEntity = cartController.fetchAllCartItems();

        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().get(0).getItems()).isSameAs(cartItemDTOS);
    }

    @Test
    @DisplayName("Should respond with expected success message")
    void fetch_all_cart_items_should_respond_with_expected_success_message() {
        List<CartItem> cartItems = new ArrayList<>();
        CartItemsResponseDTO cartItemDTOS = new CartItemsResponseDTO();

        when(cartService.getCartItems()).thenReturn(cartItems);
        when(responseMapper.map(cartItems)).thenReturn(cartItemDTOS);

        ResponseEntity<List<ResponseDTO<CartItemsResponseDTO>>> responseEntity = cartController.fetchAllCartItems();

        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().get(0).getMessage()).isSameAs("Item available in the cart");
    }

    @Test
    @DisplayName("Product should be added to cart using cart service")
    void add_product_to_cart_using_cart_service() {
        AddCartItemRequest addCartItemRequest = new AddCartItemRequest();
        addCartItemRequest.setProductId(100);
        addCartItemRequest.setQuantity(5);

        cartController.addCartItem(addCartItemRequest);

        verify(cartService).addCartItem(addCartItemRequest);
    }

    @Test
    @DisplayName("Should respond with 200-OK after successfully adding item to cart")
    void should_respond_with_http_ok_after_cart_item_is_added() {
        AddCartItemRequest addCartItemRequest = new AddCartItemRequest();

        ResponseEntity<List<ResponseDTO<Object>>> responseEntity
                = cartController.addCartItem(addCartItemRequest);

        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @DisplayName("Should respond with success message")
    void should_repond_with_success_message() {
        AddCartItemRequest addCartItemRequest = new AddCartItemRequest();

        ResponseEntity<List<ResponseDTO<Object>>> responseEntity = cartController.addCartItem(addCartItemRequest);

        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getBody()).isNotNull();
        ResponseDTO<Object> responseDTO = responseEntity.getBody().get(0);

        assertThat(responseDTO.getStatus()).isEqualTo("success");
        assertThat(responseDTO.getMessage()).isEqualTo("Item has been added to cart");

    }

    @Test
    @DisplayName("Cart service is used to clear the cart items")
    void cart_service_is_used_to_clear_cart_items() {
        cartController.clearCart("empty_cart");

        verify(cartService).clearCart();
    }

    @Test
    @DisplayName("Should produce bad request error for an invalid action")
    void should_produce_bad_request_error_for_invalid_action() {
        ResponseEntity<List<ResponseDTO<Object>>> responseEntity = cartController.clearCart("invalid_action");

        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(responseEntity.getBody()).isNotNull().isNotEmpty();
        assertThat(responseEntity.getBody().get(0).getMessage()).isNotNull().isEqualTo("Error state. Bad request.");
        assertThat(responseEntity.getBody().get(0).getStatus()).isNotNull().isEqualTo("Error");
    }

    @Test
    @DisplayName("Should give success message after clearing cart")
    void should_give_success_message_after_clearing_cart() {
        ResponseEntity<List<ResponseDTO<Object>>> responseEntity = cartController.clearCart("empty_cart");

        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull().isNotEmpty();
        assertThat(responseEntity.getBody().get(0).getMessage())
                .isNotNull().isEqualTo("All items have been removed from the cart !");
        assertThat(responseEntity.getBody().get(0).getStatus()).isNotNull().isEqualTo("success");
    }
}
