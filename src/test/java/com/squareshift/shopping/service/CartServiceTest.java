package com.squareshift.shopping.service;

import com.squareshift.shopping.model.CartItem;
import com.squareshift.shopping.model.Product;
import com.squareshift.shopping.model.request.AddCartItemRequest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class CartServiceTest {
    @InjectMocks
    private CartService cartService;

    @Test
    @DisplayName("Should return all the cart items")
    void should_return_all_cart_items() {
        assertThat(cartService.getCartItems()).isNotNull();
        assertThat(cartService.getCartItems()).isEmpty();
    }
    
    @Test
    @DisplayName("A new cart item is added to the cart")
    void cart_item_is_added_to_the_cart() {
        AddCartItemRequest addCartItemRequest = getAddCartItemRequest();
        
        cartService.addCartItem(addCartItemRequest);
        
        assertThat(cartService.getCartItems()).isNotNull();
        assertThat(cartService.getCartItems()).hasSize(1);
    }

    @Test
    @DisplayName("Item quantity is mapped")
    void cart_item_quantity_is_mapped() {
        AddCartItemRequest addCartItemRequest = getAddCartItemRequest();

        cartService.addCartItem(addCartItemRequest);

        assertThat(cartService.getCartItems()).isNotNull();
        assertThat(cartService.getCartItems()).hasSize(1);
        assertThat(cartService.getCartItems().get(0).getQuantity()).isEqualTo(10);
    }

    @Test
    @DisplayName("Item description is mapped")
    void cart_item_description_is_mapped() {
        AddCartItemRequest addCartItemRequest = getAddCartItemRequest();
        cartService.addCartItem(addCartItemRequest);

        assertThat(cartService.getCartItems()).isNotNull();
        assertThat(cartService.getCartItems()).hasSize(1);

        Product product = cartService.getCartItems().get(0).getProduct();
        assertThat(product.getId()).isEqualTo(addCartItemRequest.getProductId());
    }

    @Test
    @DisplayName("Cart items should be cleared")
    void cart_items_should_be_cleared() {
        List<CartItem> cartItems = cartService.getCartItems();

        assertThat(cartItems).isNotNull().isEmpty();
        cartService.addCartItem(getAddCartItemRequest());
        assertThat(cartItems).isNotEmpty();

        cartService.clearCart();
        assertThat(cartItems).isNotNull().isEmpty();
    }

    private AddCartItemRequest getAddCartItemRequest() {
        AddCartItemRequest addCartItemRequest = new AddCartItemRequest();
        addCartItemRequest.setProductId(110);
        addCartItemRequest.setQuantity(10);

        return addCartItemRequest;
    }
}