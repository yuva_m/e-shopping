package com.squareshift.shopping.service;

import com.squareshift.shopping.entity.ShippingCost;
import com.squareshift.shopping.repository.ShippingCostRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ShippingCostServiceTest {
    @Mock
    private ShippingCostRepository shippingCostRepository;

    @InjectMocks
    private ShippingCostService shippingCostService;

    @Test
    @DisplayName("Cost is 15 dollars for a weight of 2 kilos and distance of 6kms")
    void cost_is_15_dollars_for_a_weight_of_2_kilos_and_distance_of_6_kms() {
        Long weight = 2000L;
        Long distance = 6000L;
        BigDecimal expectedCost = new BigDecimal(15);

        when(shippingCostRepository.findAll()).thenReturn(getShippingCostListStub());

        Optional<BigDecimal> shippingCost = shippingCostService.getShippingCost(weight, distance);

        assertThat(shippingCost).isNotNull();
        assertTrue(shippingCost.isPresent());
        assertThat(shippingCost.get())
                .usingComparator(BigDecimal::compareTo)
                .isEqualTo(expectedCost);
    }

    @Test
    @DisplayName("Cost is 18 dollars for a weight of 3 kilos and distance of 6kms")
    void cost_is_18_dollars_for_a_weight_of_3_kilos_and_distance_of_6_kms() {
        Long weight = 3000L;
        Long distance = 6000L;
        BigDecimal expectedCost = new BigDecimal(18);

        when(shippingCostRepository.findAll()).thenReturn(getShippingCostListStub());

        Optional<BigDecimal> shippingCost = shippingCostService.getShippingCost(weight, distance);

        assertThat(shippingCost).isNotNull();
        assertTrue(shippingCost.isPresent());
        assertThat(shippingCost.get())
                .usingComparator(BigDecimal::compareTo)
                .isEqualTo(expectedCost);
    }

    @Test
    @DisplayName("Should return empty by default")
    void should_return_empty_by_default() {
        Optional<BigDecimal> shippingCost = shippingCostService.getShippingCost(0L, 0L);

        assertFalse(shippingCost.isPresent());
    }

    private List<ShippingCost> getShippingCostListStub() {
        List<ShippingCost> shippingCosts = new ArrayList<>();

        shippingCosts.add(getShippingCostStub(1000L, 0, 2000,
                5000, 19999, 15));
        shippingCosts.add(getShippingCostStub(1000L, 2001, 5000,
                5000, 19999, 18));

        return shippingCosts;
    }

    private ShippingCost getShippingCostStub(long id, int minimumWeight, int maximumWeight,
                                             int minimumDistance, int maximumDistance, int cost) {
        ShippingCost shippingCost = new ShippingCost();

        shippingCost.setId(id);
        shippingCost.setMinimumWeight(minimumWeight);
        shippingCost.setMaximumWeight(maximumWeight);
        shippingCost.setMinimumDistance(minimumDistance);
        shippingCost.setMaximumDistance(maximumDistance);
        shippingCost.setCost(cost);

        return shippingCost;
    }
}
